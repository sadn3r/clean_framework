<?php
$routes = 
[
	'/path/to/action' => [
		'controller' => 'home',
		'action' => 'index'
	],
	
	'/news/(\d+)/update' => [
		'controller' => 'home',
		'action' => [
			'POST'=> 'news_update'
		]
	],
];