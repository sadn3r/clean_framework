-- Adminer 4.4.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `acl`;
CREATE TABLE `acl` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `controller_action_role_id` (`controller`,`action`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `acl` (`id`, `controller`, `action`, `role_id`) VALUES
(11,	'core\\blog',	'index',	1),
(12,	'core\\blog',	'index',	2),
(13,	'core\\blog',	'post',	1),
(14,	'core\\blog',	'post',	2),
(20,	'core\\comments',	'send',	2),
(2,	'core\\home',	'index',	1),
(4,	'core\\home',	'index',	2),
(9,	'core\\home',	'search',	1),
(10,	'core\\home',	'search',	2),
(17,	'core\\home',	'timestamp',	1),
(18,	'core\\home',	'timestamp',	2),
(7,	'core\\pages',	'view',	1),
(8,	'core\\pages',	'view',	2),
(3,	'core\\responses',	'not_found',	1),
(5,	'core\\responses',	'not_found',	2),
(22,	'core\\sign',	'logout',	2),
(1,	'core\\sign',	'signin',	1),
(6,	'core\\sign',	'signin',	2),
(21,	'core\\sign',	'signup',	1),
(23,	'core\\users',	'account',	2),
(15,	'core\\users',	'view',	1),
(16,	'core\\users',	'view',	2);

DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content_short` text NOT NULL,
  `content_full` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `tags` json NOT NULL,
  `status` int(1) unsigned NOT NULL DEFAULT '1',
  `views` bigint(20) unsigned NOT NULL DEFAULT '0',
  `created_at` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `posts_comments`;
CREATE TABLE `posts_comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `content` text NOT NULL,
  `created_at` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `posts_comments_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `posts_comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `posts_views`;
CREATE TABLE `posts_views` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) unsigned NOT NULL,
  `ip` int(10) unsigned NOT NULL,
  `created_at` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `posts_views_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `created_at` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` char(40) NOT NULL,
  `role` int(10) unsigned NOT NULL DEFAULT '2',
  `username` varchar(255) NOT NULL,
  `created_at` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `login` (`login`),
  KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2018-01-20 02:09:43
