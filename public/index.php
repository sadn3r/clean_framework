<?php
namespace core;

error_reporting(0);
ini_set('display_errors', "0");

/**
 * set locale to preferred language, maybe need install locale in OS
 **/
setlocale(LC_ALL, 'ru_RU.UTF-8');


//set_include_path('../');
//session_start();

include '../config.php';
include '../routes.php';
include '../core/functions.php';
include '../core/overloading.php';
include '../vendor/autoload.php';




set_error_handler('error_handler_thrower');
register_shutdown_function('shutdown_handler');


$matches    = array();
$controller = 'responses';
$action     = 'not_found';

foreach ($routes as $route => $handler) {

	if (preg_match('#^'.$route.'$#u', rawurldecode($_SERVER['REQUEST_URI']), $matches)) {

		
		if (is_array($handler['action'])) {
			if (!isset($handler['action'][$_SERVER['REQUEST_METHOD']])) {
				continue;
			}
			
			$action = $handler['action'][$_SERVER['REQUEST_METHOD']];
		} else {
			$action = $handler['action'];
		}

		array_shift($matches);
		$controller = $handler['controller'];

		break;
	}
}

$cname = __NAMESPACE__.'\\'.$controller;
call_user_func_array([new $cname($action), $action], $matches);