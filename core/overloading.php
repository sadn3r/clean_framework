<?php
namespace core;

function trim($string = '')
{
	return \trim($string, "\xC2\xA0\x09\x20\x0A\x0D\x00\x0B");
}