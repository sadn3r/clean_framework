<?php



function get_ip()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function translit($str) {
	$rus = array(
	'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й',
	'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф',
	'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я',
	'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й',
	'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
	'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'
	);
	$lat = array(
	'A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y',
	'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F',
	'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu',
	'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i',
	'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f',
	'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya'
	);
	
	return str_replace($rus, $lat, $str);
}

function str2url($string)
{
	if (preg_match('/[^a-z0-9\:ÀÉéö\'\,\?\!\.\- ]/ui', $string)) {
		die($string);
	}

	
	$string = str_replace(array(' ',':','É','é','\'',',','À','?','.','ö','!'), array('-','','E','e','','-','A','','','o',''), $string);
	$string = preg_replace('/\-+/ui', '-', $string);
	return mb_strtolower($string);
}

function file_transfer($path, $filename)
{
	header('Content-Description: File Transfer');
	header('Content-Type: audio/mpeg');
	header('Content-Disposition: attachment; filename='.$filename);
	header('Content-Transfer-Encoding: binary');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Pragma: public');
	ob_clean();
	flush();

	echo file_get_contents($path);
	die;
}


function generate_password($size = 5)
{
	$chars		= "1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
	$chars_size = mb_strlen($chars) - 1;

	$password	= null;
	while ($size--) {
		$password .= $chars[rand(0,$chars_size)]; 
	}

	return $password;
}


function db()
{
	static $db;

	if (!$db)
	{
	
		$driver = new \mysqli_driver();
		$driver->report_mode = MYSQLI_REPORT_ALL;

		try {
			$db = new \core\db(db_addr, db_user, db_pass, db_name, db_sock? 0 : db_port, db_sock);
			$db->set_charset('utf8');
			$db->options(MYSQLI_OPT_INT_AND_FLOAT_NATIVE, true);
		} catch (Exception $e) {
			throw new Exception("error connect db", 1);
		}

	}

	return $db;
}


function mdb()
{
	static $mdb;
	
	if (!$mdb)
	{
		$mdb = new \Redis;
		$mdb->connect(redis_server);
	}

	return $mdb;
}


function location($url)
{
	header('Location: '.$url);
	die;
}


function error_handler_thrower($severity, $message, $filename, $lineno, $dd)
{
	throw new \ErrorException($message, 0, $severity, $filename, $lineno); 
}

function shutdown_handler()
{
    if ($error = error_get_last()) {

        var_dump($error);
        die;
    }	
}


/**
 * Возвращает язык пользователя указанный в настройках браузера по умолчанию.
 **/
function get_default_lang()
{
	$default_lang	= 'ru';
	$accept_lang	= $_SERVER['HTTP_ACCEPT_LANGUAGE'];
	if (!$accept_lang) {
		return $default_lang;
	}
	$accept_lang = mb_strtolower($accept_lang);
	
	if (!preg_match_all('/([a-z]{1,8}(?:-[a-z]{1,8})?)(?:;q=([0-9.]+))?/', $accept_lang, $list)) {
		return $default_lang;	
	}

	$lang = preg_replace('/\-.*/', '', $list[1][0]);
	return $lang;
}


function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;

    $ago = new DateTime(date('d.m.Y H:i:s',$datetime));
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = [
        'y' => ['год','года','лет'],
        'm' => ['месяц', 'месяца', 'месяцев'],
        'w' => ['неделю', 'недели', 'недель'],
        'd' => ['день','дня','дней'],
        'h' => ['час','часа','часов'],
        'i' => ['минуту','минуты', 'минут'],
        's' => ['секунду','секунды','секунд'],
    ];

    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . plural_form($diff->$k, $v);
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' назад' : 'только что';
}

function plural_form($n, $forms) {
	return $n%10==1&&$n%100!=11?$forms[0]:($n%10>=2&&$n%10<=4&&($n%100<10||$n%100>=20)?$forms[1]:$forms[2]);
}

function metaphone_rus($s)
{
	if (is_array($s)) {
		$mask = sizeof($s)==1?$s[0]:$s[1];

		$mask = str_replace(
			['йо','ио','йе','ие','о','ы','я','е','ё','э','ю','б','з','д','в','г'],
			['и' ,'и' ,'и' ,'и' ,'а','а','а','и','и','и','у','п','с','т','ф','к'], $mask);

		return isset($s[2]) ? $mask.$s[2] : $mask;
	}

	$s = mb_strtolower(trim($s));
	$s = preg_replace('/([а-я])\\1/ui', '$1', $s);
	$s = preg_replace_callback('/[бздвг]$/ui', __function__, $s);
	$s = preg_replace_callback('/йо|ио|йе|ие|[оыяеёэю]/ui', __function__, $s);
	$s = preg_replace_callback('/([бздвг])([бвгджзкпстфхцчшщ])/ui', __function__, $s);
	$s = preg_replace('/([а-я])\\1/ui', '$1', $s);
	$s = preg_replace('/тс|дс/ui', 'ц', $s);
	return $s;
}