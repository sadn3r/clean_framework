<?php
namespace core;
class responses extends controller
{
	function not_found($template = '404')
	{
		header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
		$this->render($template);
		die;
	}
}