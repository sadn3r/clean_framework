<?php
namespace core;
use \Firebase\JWT\JWT;

class sign extends controller
{
	
	function signup()
	{

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

			$password		= generate_password(4);
			$password_hash	= sha1($password.user_salt);

			$id = db()->insert('users', [
				'login'			=> $_REQUEST['login'],
				'password'		=> $password_hash,
				'role'			=> 2,
				'created_at'	=> time(),
			]);

			$this->render_json([
				'password'	=> $password,
				'status'	=> 2,
			]);
		}

		$this->js[] = ['type' => 'script', 'content' => 'signup.js'];
		$this->page_title = 'Регистрация';
		$this->crumbs = breadcrumbs([
			'/'			=> 'Главная',
			'/signup'	=> 'Регистрация',
		]);		
		$this->render('signup');
	}


	function signin()
	{

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			
			$login		= trim($_REQUEST['login']);
			$password	= sha1($_REQUEST['password'].user_salt);

			$_r = db()->query('
			select
				id,
				role,
				login
			from users
			where
				login = "'.db()->escape_string($login).'" and
				password = "'.db()->escape_string($password).'"
			');

			$user = $_r->fetch_assoc();
			
			if ($user) {

				$token = [
					'iat' => time(),
					'iss' => 'https://sadner.ru',
					'user'	=> [
						'id'	=> $user['id'],
						'role'	=> $user['role'],
						'login'	=> $user['login'],
					]
				];

				$jwt = JWT::encode($token, jwt_key);

				setcookie('ua', $jwt, time()+60*60*24*365, '/');
				$this->render_json([
					'status'	=> 1,
				]);				
			}

			$this->render_json([
				'status' => 2
			]);

		}

		$this->js[] = ['type' => 'script', 'content' => 'signin.js'];
		$this->crumbs = breadcrumbs([
			'/'			=> 'Главная',
			'/signin'	=> 'Войти на сайт',
		]);

		$this->page_title = 'Войти на сайт';
		$this->render('signin');
	}

	function logout()
	{
		$logouted_user = $this->user;
		$this->user = null;
		
		setcookie('ua', '', -1, '/');
		$this->render('logout', [
			'logouted_user'	=> $logouted_user
		]);
	}

}