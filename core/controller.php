<?php
namespace core;
use \Firebase\JWT\JWT;

abstract class controller
{
	
	static public $lang	= 'ru';
	public $user;


	static function get_user_session()
	{
		if (!isset($_COOKIE['ua'])) {
			return null;
		}
		
		try {
			$decoded = JWT::decode($_COOKIE['ua'], jwt_key, array('HS256'));
		} catch (\UnexpectedValueException $e) {
			setcookie("ua", "", 1, '/');
			return null;
		}

		return (array)$decoded->user;
	}

	function __construct($action)
	{	
		
		$this->user = self::get_user_session();
		if ($this->user) {
			$role = $this->user['role'];
		} else {
			$role = 1;	// guest 
		}
		

		if (preg_match('/^(?<lang>en)\.sadner\.ru$/ui', $_SERVER['HTTP_HOST'], $langs)) {
			self::$lang = $langs['lang'];
		}

		
		/*
		$_res = db()->query('
			select
				a.role_id
			from acl a
			where
				a.controller = "'.db()->escape_string(get_class($this)).'" and
				a.action = "'.db()->escape_string($action).'" and
				a.role_id = '.$role.'
		');

		if (!$_res->num_rows) {
			location('/signin');
		}
		*/
	}


	static function render_json(array $data = [])
	{
		header("Content-type: application/json; charset=utf-8");
		die(json_encode($data));
	}
	

	function mail($to, $subj, $body)
	{
		$headers	= array(
			'MIME-Version: 1.0',
			'From: ' . admin_email,
			'Reply-To: ' . admin_email,
			'Content-Type: text/html; charset=utf-8'
		);

		return mail($to, $subj, $body, implode("\r\n", $headers));
	}

}