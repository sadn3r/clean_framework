<?php
namespace core;

class db extends \mysqli {
	function exec($statement, $data)
	{

		$new_statement = preg_replace_callback('/([i|s])\:([a-z]+)/ui', function($matches) use ($data) {

			switch ($matches[1]) {
				case 'i':
					return (int)$data[$matches[2]];
				break;
				case 's':
					return $this->escape_string($data[$matches[2]]);
				break;
			}

		}, $statement);

		return $this->query($new_statement);
	}
}