<?php

define('debug', true);

define('project', 'clean_framework');


// database
// if sock defined, port not using, connect via socket
define('db_addr', 'localhost');
define('db_user', '');
define('db_pass', '');
define('db_name', '');
define('db_port', 3306);
define('db_sock', '/var/run/mysqld/mysqld.sock');

// redis
define('redis_server', 'localhost:6379');


// security
define('user_salt', '7856fgzz');
define('jwt_key', 'dgd27fnmnxbc78755111poizxcb');

define('recaptcha_key', '6LeKc0EUAAAAANkzfRckkQWi9LT3Yqig4vkHSXI9');
define('recaptcha_secretkey', '6LeKc0EUAAAAAHzHTrXMBeG27geMXE_GMPswPLVH');



// mail
define('admin_email', 'noreply@sadner.ru');