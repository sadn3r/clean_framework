<?php
namespace core;

set_include_path('../');
require '../config.php';
require '../core/functions.php';

spl_autoload_register();
set_error_handler('error_handler_thrower');
register_shutdown_function('shutdown_handler');

// do cron action